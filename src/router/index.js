import Vue from 'vue'
import Router from 'vue-router'

import Home from '@/components/Home'
import ErrorPg from '@/components/Error'
import Car from '@/components/Car';

Vue.use(Router)

export default new Router({
    routes: [
        {path: '/', component: Home},
        {path: '/genre', component: Car},
        {path: '/music', component: Home},
        {path: '*', component: ErrorPg}
    ],
    mode: 'history'
})